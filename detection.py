import numpy as np
import cv2
from os.path import join
from keras.models import Sequential
from keras.layers import Conv2D, MaxPooling2D
from keras.optimizers import SGD
from keras.layers import Dense, Dropout, Flatten, Activation
import keras
from keras import metrics
from keras import losses


POINTSNUM = 14
SPLITDATA = 25  # (1 / SPLITDATA) pictures in validation set
INPUTSHAPE = (100, 100)


def create_net3():
    model = Sequential()
    model.add(Conv2D(32, (3, 3), padding="valid", input_shape=(INPUTSHAPE[0], INPUTSHAPE[1], 3)))
    model.add(Activation("relu"))
    model.add(MaxPooling2D(pool_size=(2, 2)))

    model.add(Conv2D(64, (2, 2), padding="valid"))
    model.add(Activation("relu"))
    model.add(MaxPooling2D(pool_size=(2, 2)))

    model.add(Conv2D(128, (2, 2), padding="valid"))
    model.add(Activation("relu"))
    model.add(MaxPooling2D(pool_size=(2, 2)))

    model.add(Flatten())  # make output from previous layer 1-dimensional
    model.add(Dense(500))
    model.add(Activation("relu"))

    model.add(Dense(500))
    model.add(Activation("relu"))
    
    model.add(Dense(POINTSNUM * 2))
    return model


def train_detector(trainPnts, trainImgDir, fast_train=False):
    model = create_net3()
    model.compile(loss=losses.mean_squared_error, optimizer=keras.optimizers.Adadelta())
    
    files = np.array(list(trainPnts.keys()))
    np.random.shuffle(files)
    pics = np.array([cv2.imread(join(trainImgDir, f)) for f in files])
    points = np.array([trainPnts[files[i]] for i in range(files.shape[0])])
    
    validation = pics[int(pics.shape[0] * ((SPLITDATA - 1) / SPLITDATA)) :]
    pointsVal = points[int(points.shape[0] * ((SPLITDATA - 1) / SPLITDATA)) :]
    
    train = pics[: int(pics.shape[0] * ((SPLITDATA - 1) / SPLITDATA))]
    pointsTr = points[: int(points.shape[0] * ((SPLITDATA - 1) / SPLITDATA))]

    fIn = open("Error MSE Adadelta 100x100 not norm 2", 'w')

    batchSize = 64
    for ep in range(100):
        trLosses = np.array([], dtype='Float64')
        for s in range(train.shape[0] // batchSize + (train.shape[0] % batchSize != 0)):
            batch = train[s * batchSize : (s + 1) * batchSize]
            batchPoints = pointsTr[s * batchSize : (s + 1) * batchSize]
            trLosses = np.append(trLosses, model.train_on_batch(batch, batchPoints))
        print("Mean loss on epoch %d is: (tr) %f\t" % (ep + 1, trLosses.mean()), end=' ')
        print("Mean loss on epoch %d is: (tr) %f\t" % (ep + 1, trLosses.mean()), end=' ', file=fIn)

        # validation
        valLosses = np.array([], dtype='Float64')
        for s in range(validation.shape[0] // batchSize + 1):
            batch = validation[s * batchSize : (s + 1) * batchSize]
            batchPoints = pointsVal[s * batchSize : (s + 1) * batchSize]
            valLosses = np.append(valLosses, model.test_on_batch(batch, batchPoints))
        print("%f (val)" % valLosses.mean())
        print("%f (val)" % valLosses.mean(), file =fIn)
    fIn.close()
    return model


def detect(model, testImgDir):
    pred = model.predict(test, batch_size=1)
    
    output = {}
    for i in range(len(testImgDirList)):
        output[testImgDirList[i]] = pred[i]
    return output

